//
//  InfoTableViewCell.swift
//  DuoHealtware
//
//  Created by Santiago Lema on 22/6/17.
//  Copyright © 2017 EPN. All rights reserved.
//

import UIKit

class InfoTableViewCell: UITableViewCell {
    

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var detailLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func llenarCelda(name: String, detail: String)
    {
        
        
        nameLabel.text = name
        detailLabel.text = detail
    }

}
