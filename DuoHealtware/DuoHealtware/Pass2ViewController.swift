//
//  Pass2ViewController.swift
//  DuoHealtware
//
//  Created by Santiago Lema on 28/6/17.
//  Copyright © 2017 EPN. All rights reserved.
//

import UIKit
import SmileLock

class Pass2ViewController: UIViewController {

    @IBOutlet weak var passwordStackView: UIStackView!
    var passwordContainerView: PasswordContainerView!
    let kPasswordDigit = 4
    override func viewDidLoad() {
        
        super.viewDidLoad()
      
        passwordContainerView = PasswordContainerView.create(in: passwordStackView, digit: kPasswordDigit)
        passwordContainerView.delegate = self as! PasswordInputCompleteProtocol
        passwordContainerView.deleteButtonLocalizedTitle = "Delete"
        
        //customize password UI
        passwordContainerView.tintColor = UIColor.black
        passwordContainerView.highlightedColor = UIColor.blue
        
      
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let value = UIInterfaceOrientation.portrait.rawValue
        UIDevice.current.setValue(value, forKey: "orientation")
    }
    
   }

extension Pass2ViewController: PasswordInputCompleteProtocol {
    func passwordInputComplete(_ passwordContainerView: PasswordContainerView, input: String) {
        if validation(input) {
            validationSuccess()
        } else {
            validationFail()
        }
    }
    
    func touchAuthenticationComplete(_ passwordContainerView: PasswordContainerView, success: Bool, error: Error?) {
        if success {
            self.validationSuccess()
        } else {
            passwordContainerView.clearInput()
        }
    }
}

private extension Pass2ViewController {
    func validation(_ input: String) -> Bool {
        return input == "1234"
    }
    
    func validationSuccess() {
        print("*️⃣ success!")
        performSegue(withIdentifier: "segue2", sender: self)
      
        
            }
    
    func validationFail() {
        print("*️⃣ failure!")
        
    performSegue(withIdentifier: "segue2", sender: self)
    }
    
    
}
