//
//  ViewController.swift
//  DuoHealtware
//
//  Created by Santiago Lema on 20/6/17.
//  Copyright © 2017 EPN. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    let infoKey = Array(Info.keys).sorted()
    
    
    @IBOutlet weak var profileImage: UIImageView!
    

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.profileImage.layer.cornerRadius = self.profileImage.frame.size.width / 2;
        self.profileImage.clipsToBounds = true
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Info.count-2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! InfoTableViewCell
        
    
        let nombre = infoKey[indexPath.row]
        
        let nombre2 = String(nombre.characters.dropFirst())
        
        let detalle = Info[nombre]

        cell.llenarCelda(name: nombre2, detail: detalle!)
        
        return cell

}
    
    
    @IBAction func salirAqui(segue: UIStoryboardSegue)
    {
        
    }
}


    



