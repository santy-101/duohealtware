//
//  Persona.swift
//  DuoHealtware
//
//  Created by Santiago Lema on 22/6/17.
//  Copyright © 2017 EPN. All rights reserved.
//

import Foundation
import UIKit

class Persona{
    var firstName:String
    var lastName:String
    var height:String
    var country: String
    var email: String
    var phone: String
    var password: String
    var pin: String

    init(firstName:String, lastName:String, height: String, country:String, email: String, phone: String, password: String, pin: String ) {
        self.firstName = firstName
        self.lastName = lastName
        self.height = height
        self.country = country
        self.email = email
        self.phone = phone
        self.password = password
        self.pin = pin
       
    }
    
    
}
